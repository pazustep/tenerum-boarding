package com.tenerum.boarding.manager;

import com.tenerum.boarding.data.BoardingEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Stateless
@Transactional(Transactional.TxType.MANDATORY)
public class BoardingEntityManager {
    @PersistenceContext(unitName = "boarding")
    private EntityManager entityManager;

    public BoardingEntity find(Integer id) {
        List<BoardingEntity> entities = find(Collections.singletonList(id));
        return entities.isEmpty() ? null : entities.get(0);
    }

    public List<BoardingEntity> find(List<Integer> ids) {
        List<BoardingEntity> entities = entityManager.createQuery("" +
            "SELECT e FROM BoardingEntity e WHERE e.id IN :ids", BoardingEntity.class)
            .setParameter("ids", ids)
            .getResultList();

        Map<Integer, BoardingEntity> entityMap = entities.stream()
            .collect(toMap(BoardingEntity::getId, Function.identity()));

        return  ids.stream().map(entityMap::get).collect(toList());
    }
}
