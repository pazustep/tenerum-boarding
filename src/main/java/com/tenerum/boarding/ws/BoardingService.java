package com.tenerum.boarding.ws;

import com.tenerum.boarding.data.BoardingEntity;
import com.tenerum.boarding.manager.BoardingEntityManager;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.transaction.Transactional;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@WebService(
    portName = "BoardingPort",
    serviceName = "BoardingService",
    targetNamespace = "http://www.tenerum.com/v1/boarding/ws/data")
@Transactional
public class BoardingService {
    @Inject
    private BoardingEntityManager boardingEntityManager;

    @WebMethod
    @WebResult(name = "boardingEntity")
    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    public BoardingEntity find(@WebParam(name = "entityId") @XmlElement(nillable = false) Integer id) {
        return boardingEntityManager.find(id);
    }

    @WebMethod
    @WebResult(name = "boardingEntity")
    @XmlElement(nillable = true)
    public List<BoardingEntity> findMany(@WebParam(name = "entityId") @XmlElement(required = true) List<Integer> ids) {
        return boardingEntityManager.find(ids);
    }
}
